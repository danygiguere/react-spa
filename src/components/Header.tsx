import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link, 
} from "react-router-dom";
import loadable from '@loadable/component';
import axios from 'axios';
import { deleteToken } from '../auth';

const WelcomeComponent = loadable(() => import('../pages/Welcome'));
const PostsIndexComponent = loadable(() => import('../pages/posts/index'));
const PostsShowComponent = loadable(() => import('../pages/posts/show'));
const UsersIndex = loadable(() => import('../pages/users/index'));
const UsersShow = loadable(() => import('../pages/users/show'));
const RegisterComponent = loadable(() => import('../pages/auth/Register'));
const LoginTsxComponent = loadable(() => import('../pages/auth/Login'));
const FourOFour = loadable(() => import('../pages/errors/FourOFour'));

const Header: React.FC = () => {

  const logout = (event:React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    axios.post('http://127.0.0.1:3333/api/logout').then(response => {
      deleteToken()
      // redirect to home page    
    }).catch(error => {
      console.log('error', error);
    })
  }

  return <Router>
      <div className="container mx-auto">
        <ul>
          <li>
            <Link to="/">Welcome</Link>
          </li>
          <li>
            <Link to="/posts">Posts</Link>
          </li>
          <li>
            <Link to="/users">Users</Link>
          </li>
          <li>
            <Link to="/register">Register</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <form className="w-full max-w-sm" onSubmit={logout}>
              <button className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                Logout
              </button>
            </form>
          </li>
        </ul>

        <hr />

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
        <Route exact path="/">
            <WelcomeComponent />
          </Route>
          <Route exact path="/posts/*">
            <PostsShowComponent />
          </Route>
          <Route exact path="/posts">
            <PostsIndexComponent />
          </Route>
          <Route path="/users/*">
            <UsersShow />
          </Route>
          <Route path="/users">
            <UsersIndex />
          </Route>
          <Route path="/register">
            <RegisterComponent />
          </Route>
          <Route path="/login">
            <LoginTsxComponent />
          </Route>
          <Route path="*">
            <FourOFour />
          </Route>
        </Switch>
      </div>
    </Router>
}

export default Header