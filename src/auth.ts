import axios from 'axios';

export const getToken = () => {
    const tokenString: string | null = localStorage.getItem(`auth_token`);
    return tokenString ? JSON.parse(tokenString) : undefined;
}

export const setToken = (token: string, expires_at: string): void => {
    localStorage.setItem('auth_token', JSON.stringify(`Bearer ${token}`));
    localStorage.setItem('auth_expires_at', JSON.stringify(expires_at));
    axios.defaults.headers.common['Authorization'] = getToken();
}

export const deleteToken = (): void => {
    localStorage.removeItem('auth_token')
    localStorage.removeItem('auth_expires_at')
}