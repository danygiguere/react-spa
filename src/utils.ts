export const getPostIdFromUrl = () => {
    let pathname = window.location.pathname;
    return pathname.replace('/posts/', '');
}

export const getUserIdFromUrl = () => {
    let pathname = window.location.pathname;
    return pathname.replace('/users/', '');
}