import React from 'react';
import './App.css';
import loadable from '@loadable/component';
import { getToken } from './auth';
import axios from 'axios';

axios.defaults.headers.common['Authorization'] = getToken();


const HeaderComponent = loadable(() => import('./components/Header'));

function App() {
  const token = getToken();
  if(!token) {
    console.log('redirect to login page');
  }

  return (
    <HeaderComponent />
  );
}

export default App;