import React, { useState } from 'react'
import axios from 'axios';
import {
  Link
} from "react-router-dom";
import { getUserIdFromUrl } from '../../utils';

interface Post {
  id: number, 
  title: string, 
  description: string
}

interface User {
  id: number, 
  first_name: string, 
  last_name: string,
  posts: Post[] | undefined
}

const UsersShow: React.FC = () => {
  const [user, setUser] = useState<User>();

  React.useEffect(() => {
    axios.get(`http://127.0.0.1:3333/en/api/users/${getUserIdFromUrl()}`)
    .then(response => {
      setUser(response.data);
    })
  }, [])

  return <div>
            <h2>UsersShow</h2> 
            <h3>FirstName: { user && user.first_name }</h3> 
            <h3>LastName: { user && user.last_name }</h3> 
            <h2>UsersPosts</h2> 

            {user && user.posts && user.posts.map((post) => (
              <><li><Link to={{
                pathname: `/posts/${post.id}`
              }}>{post.title}</Link></li></>
            ))}
         </div>
}
export default UsersShow