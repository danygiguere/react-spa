import React, { useState } from 'react'
import axios from 'axios';
import {
  Link
} from "react-router-dom";

interface User {
  id: number, 
  first_name: string, 
  last_name: string,
  email: string
}

const UsersIndex: React.FC = () => {
  
  const [users, setUsers] = useState<User[]>([]);

  React.useEffect(() => {
    axios.get(`http://127.0.0.1:3333/en/api/users`)
    .then(response => {
      setUsers(response.data);
    })
  }, [])

  return <div>
    <h2>UsersIndex</h2> 
    <ul>
        {users.map((user) => (
          <><li><Link to={{
            pathname: `/users/${user.id}`
          }}>{user.email}</Link></li></>
        ))}
    </ul>
  </div>
}
export default UsersIndex