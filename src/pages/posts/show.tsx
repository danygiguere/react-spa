import React, { useState } from 'react'
import axios from 'axios';
import { getPostIdFromUrl } from '../../utils';

interface Post {
  title: string, 
  description: string
}

const PostsShow: React.FC = () => {
  const [post, setPost] = useState<Post | undefined>();

  React.useEffect(() => {
    axios.get(`http://127.0.0.1:3333/en/api/posts/${getPostIdFromUrl()}`)
    .then(response => {
      setPost(response.data);
    })
  }, [])

  return <div>
    <h2>PostsShow</h2> 
    <h3>Title: { post && post.title }</h3> 
    <h3>Title: { post && post.description }</h3> 
  </div>
}
export default PostsShow