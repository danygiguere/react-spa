import React, { useState } from 'react'
import axios from 'axios';

interface User {
  id: number, 
  first_name: string, 
  last_name: string,
}

interface Post {
  id: number, 
  title: string, 
  description: string,
  user: User | undefined
}

const PostsIndex: React.FC = () => {
  
  const [posts, setPosts] = useState<Post[]>([]);

  React.useEffect(() => {
    axios.get(`http://127.0.0.1:3333/en/api/posts`)
    .then(response => {
      console.log(response)
      setPosts(response.data);
    })
  }, [])

  return <div className='container mx-auto'>
    <div className='grid grid-cols-6 gap-4 mt-5 mb-5'>
      <div className='col-span-4'>
      {posts.map((post) => (
          <>
            <article className='mb-5 border rounded-lg  border-gray-200'>
              <div className='p-2.5'>
                <p>{post.user?.first_name}</p>
              </div>
              <img className='sm:flex-row flex-col' src="https://images.unsplash.com/photo-1640645332870-d8b9f11eae6b" alt="" />
              <p>{post.title}</p>
            </article>
          </>
        ))}
      </div>
      <div className='col-span-2'>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit temporibus dignissimos sit commodi nam a, cumque rerum rem ad. Qui voluptas molestiae labore repudiandae id quos suscipit provident, iure ipsa?</p>
      </div>
    </div>
  </div>
}
export default PostsIndex