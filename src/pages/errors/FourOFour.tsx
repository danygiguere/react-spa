import React, { Component } from 'react'

export default class FourOFour extends Component {
  render() {
    return (
      <div>
        <h3>
          404 Sorry this page doesn't exist
        </h3>
      </div>
    )
  }
}