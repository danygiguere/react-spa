## Installation
- npm install
- yarn start

## Source of information
- integrate https://tailwindcss.com/docs/guides/create-react-app
- https://react-typescript-cheatsheet.netlify.app/docs/basic/getting-started/class_components/
- https://codersera.com/blog/react-hooks-with-typescript-use-state-and-use-effect-in-2020/
- https://www.bezkoder.com/react-typescript-axios/
- https://esbuild.github.io/


## Tricks
To figure out an type, for insance on an event for onSubmit, you can hover over onSubmit and copy the type in the popup

## TODO
